package main

import (
	"bufio"
	"compress/gzip"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
)

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type arrayFlags []string

func (i *arrayFlags) String() string {
	return "my string representation"
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

//Filereader takes a file and reads data onto a particular data channel
//Performs a buffered read to take memory into consideration
func Filereader(data chan string, filename string, wgData *sync.WaitGroup, gz *regexp.Regexp) {
	defer wgData.Done()
	file, err := os.Open(filename)
	check(err)
	defer file.Close()
	scanner := bufio.NewScanner(file)

	if gz.MatchString(filename) {
		gzF, err := gzip.NewReader(file)
		check(err)
		defer gzF.Close()
		scanner = bufio.NewScanner(gzF)
	}

	for scanner.Scan() {
		data <- scanner.Text()
	}
}

//Matcher the data for the pattern specified
func Matcher(data chan string, r *regexp.Regexp, matched chan string, wgMatched *sync.WaitGroup) {
	for line := range data {
		if r.MatchString(line) {
			matched <- line
		}
	}
	wgMatched.Done()
}

//Reads the matched channel and outputs it to the screen
func printer(matched chan string, done chan bool) {
	for line := range matched {
		fmt.Println(line)
	}
	done <- true

}

// main function, defining flags and finding matches using go routines
func main() {
	var p = flag.String("p", "", "Pattern to be searched in the text file")
	var e = flag.String("e", "", "Pattern that should be present in the starting of string")

	var lcase = flag.Bool("case", true, "Check Case")

	var file arrayFlags
	flag.Var(&file, "f", "File name of target file")

	var g = flag.Int("g", 1, "Number of Routines to employ in finding pattern")

	flag.Parse()

	var data = make(chan string)
	var matched = make(chan string)
	var wgData sync.WaitGroup
	var wgMatched sync.WaitGroup
	var done = make(chan bool)

	if !*lcase {
		*p = strings.ToLower(*p)
		*e = strings.ToLower(*e)
	}

	pattern := *p
	if *e != "" {
		pattern = "^" + *e
	}
	if !*lcase {
		pattern = "(?i)" + pattern
	}

	r := regexp.MustCompile(pattern)
	gz := regexp.MustCompile(".gz$")

	for i := 0; i < len(file); i++ {
		matches, _ := filepath.Glob(file[i])
		for _, p := range matches {
			wgData.Add(1)
			go Filereader(data, p, &wgData, gz)

		}

	}

	go func() {
		wgData.Wait()
		close(data)
	}()

	for i := 0; i < *g; i++ {
		wgMatched.Add(1)
		go Matcher(data, r, matched, &wgMatched)
	}

	go func() {
		wgMatched.Wait()
		close(matched)

	}()

	go printer(matched, done)

	<-done

}
