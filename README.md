# GGREP
Grep like utility made in Golang



### Installation:
Requirements:
- Golang 1.14

Procedure:

- Clone the Repository
```
   $ git clone https://bitbucket.org/harsh-not-haarsh/ggrep/src/master/
```
- Navigate to the Repository
```
   $ cd ggrep
```

### Running:

- Search for pattern 

```
   $ go run ggrep.go -p <text pattern you want to look for> -f file1 -f file2 -g 3
```

- Search for strings starting with pattern 
```
   $ go run ggrep.go -e <text pattern you want to look for> -f file1 -f file2 -g 3
```

- Search for pattern (Case Independent)

```
   $ go run ggrep.go -p <text pattern you want to look for> -f file1 -case=false
```

- Get Help

```
   $ go run ggrep.go -h
```