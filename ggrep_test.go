package main

import (
	"fmt"
	"os"
	"regexp"
	"sync"
	"testing"
)

func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}

func deleteFile(path string) {
	// delete file
	var err = os.Remove(path)
	if isError(err) {
		return
	}

	// fmt.Println("File Deleted")
}

func createFile(path string) {

	var file, err = os.Create(path)
	defer file.Close()
	if isError(err) {
		return
	}
	_, err = file.WriteString("Hello\n")
	if isError(err) {
		return
	}
	_, err = file.WriteString("World\n")
	if isError(err) {
		return
	}
	err = file.Sync()
	if isError(err) {
		return
	}
	// fmt.Println("File Created Successfully.")
}

func TestFileReader(t *testing.T) {
	var path = "test.txt"
	var _, err = os.Stat(path)

	if os.IsNotExist(err) {
		createFile(path)
		var data = make(chan string, 2)
		var wg sync.WaitGroup
		wg.Add(1)
		gz := regexp.MustCompile(".gz$")
		go Filereader(data, "test.txt", &wg, gz)
		wg.Wait()
		if "Hello" != <-data || "World" != <-data {
			t.Errorf("File Reader Failed")
		}
		deleteFile(path)
	} else {
		t.Errorf("file test.txt exists, remove file to proceed")
	}
	fmt.Println("File Reader Passed")
}

func TestMatcher(t *testing.T) {
	pattern := "test3"
	var wg sync.WaitGroup

	var dataL = make(chan string, 10)
	var matched = make(chan string, 10)

	for i := 0; i < 5; i++ {
		dataL <- "test" + string(i+'0')
	}
	for i := 0; i < 5; i++ {
		dataL <- "Test" + string(i+'0')
	}
	close(dataL)
	r := regexp.MustCompile(pattern)
	wg.Add(1)
	Matcher(dataL, r, matched, &wg)

	wg.Wait()
	close(matched)

	check := 0
	for val := range matched {
		if val != "test3" {
			t.Errorf("Case Insensitive Element Found, Case Sensitive search failed")
		}
		check++
	}
	if check != 1 {
		t.Errorf("Either repeating or missing elements, Case Insenitive search failed")
	}

	// Looking for errors in case insensitive search
	var dataU = make(chan string, 10)
	var matchedU = make(chan string, 10)

	for i := 0; i < 5; i++ {
		dataU <- "test" + string(i+'0')
	}
	for i := 0; i < 5; i++ {
		dataU <- "Test" + string(i+'0')
	}
	close(dataU)
	r = regexp.MustCompile("(?i)" + pattern)
	wg.Add(1)
	go Matcher(dataU, r, matchedU, &wg)
	wg.Wait()
	close(matchedU)

	check = 0
	for val := range matchedU {
		if val != "test3" && val != "Test3" {
			t.Errorf("Wrong Elements found, Case Insensitive search failed")
		}
		check++
	}
	if check != 2 {
		t.Errorf("Either repeating or missing elements, Case Insenitive search failed")
	}
	fmt.Println("Matcher Passed")

}
